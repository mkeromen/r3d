# R3d application

J'ai utilisé le micro-framework [Silex](https://silex.symfony.com/) pour la réalisation de ce petit exercice. J'ai fais ce choix car je voulais utilisé quelque chose de simple et sécurisé. Par exemple, la manipulation des requêtes http est un sujet critique concernant la sécurité sur les applications web. Ici il se fait de façon plus sécuritaire avec le composant de Symfony http-foundation.

## Installation

Pour le développement de cet exercice, j'ai crée et testé mon travail avec Docker. Le fichier `/docker/docker-compose.yml` montre la structure utilisée pour la création et le montage des différents conteneurs.

1. Récupérer les sources depuis le repo Bitbucket.
2. Se placer dans le dossier `/www/` et faire un `composer install`.
3. Configurer un serveur nginx avec le document root pointant sur le dossier `/web` du projet. 
4. Configurer une version de PHP (en FPM par exemple). 
5. Configurer un serveur MySQL puis créer une base `r3d` et executer le script `/sql/tables.sql`. Des ajustements  de configuration seront peut être nécessaires dans `/src/app.php`

## Versions utilisées
Pour l'exercice, voici mes versions de développement pour chacune des technos :
- Nginx 1.12
- PHP 7.1
- MySQL 5.7