<?php

namespace Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        return $builder->add('name', TextType::class, [
            'label' => 'Nom',
            'constraints'   =>  [
                new Assert\NotBlank(),
            ]
        ])
        ->add('description', TextareaType::class)
        ->add('price', MoneyType::class, [
            'label' => 'Prix',
            'currency' => null,
            'constraints'   =>  [
                new Assert\NotBlank(),
            ]
        ])
        ->add('submit', SubmitType::class, [
            'label' => 'Enregistrer',
        ]);
    }
        
    public function getName()
    {
        return "product";
    }
}