<?php

use Silex\Application;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;

$app = new Application();
$app->register(new ServiceControllerServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new HttpFragmentServiceProvider());
$app->register(new SwiftmailerServiceProvider());
$app->register(new DoctrineServiceProvider(), [
    'db.options' => [
        'driver'   => 'pdo_mysql',
        'host'     => '172.16.3.3',
        'dbname'   => 'r3d',
        'user'     => 'r3d_db',
        'password' => 'r3d_db',
    ]
]);
$app['twig'] = $app->extend('twig', function ($twig, $app) {
    $twig->addExtension(new TranslationExtension());
    
    return $twig;
});

return $app;
