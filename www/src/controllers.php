<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Form\ProductType;


$app->get('/', function () use ($app) {
    return $app['twig']->render('index.html.twig');
})
->bind('homepage');

$app->match('/products/add', function(Request $request) use ($app) {
    
    $form = $app['form.factory']
        ->createBuilder(ProductType::class)
        ->getForm();
    
    $form->handleRequest($request);
    if ($form->isValid()) {
        $data = $form->getData();
        
        $app['db']->insert('product', $data);
        
        $body = $app['twig']->render('email_product.html.twig', ['product' => $data]);
        
        $message = (new \Swift_Message($app['email.product.subject']))
            ->setFrom($app['email.product.from'])
            ->setTo($app['email.product.to'])
            ->setBody($body, 'text/html');
        
        $app['mailer']->send($message);
        
        return $app->redirect($app['url_generator']->generate('homepage'));
    }
    
    return $app['twig']->render('add_product.html.twig', [
        'form' => $form->createView()
    ]);
    
})
->bind('add_product')
->method('GET|POST');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    $templates = [
        'errors/'.$code.'.html.twig',
        'errors/'.substr($code, 0, 2).'x.html.twig',
        'errors/'.substr($code, 0, 1).'xx.html.twig',
        'errors/default.html.twig',
    ];

    return new Response($app['twig']->resolveTemplate($templates)->render(['code' => $code]), $code);
});
